# [shén lóng (神龙)](https://en.wikipedia.org/wiki/Shenlong)

a frontend for [elstat] that uses [react]

[elstat]: https://gitlab.com/elixire/elstat
[react]: https://reactjs.org

## installing

configure:

```sh
$ cp .env .env.local
$ $EDITOR .env.local
```

make sure `REACT_APP_SHENLONG_API_URL` points to the [elstat] backend. the
frontend will use this to make requests.

install dependencies:

```sh
$ npm install

# in a ci, do:
$ npm ci
```

finally, build:

```sh
$ npm run build
```

static files will be built to `./build`. serve this somewhere.

### example nginx config

shenlong is a [single page application][spa], so requests should fall back to
`index.html`:

[spa]: https://en.wikipedia.org/wiki/Single-page_application

```
server {
  server_name elstat.tld;

  location / {
    # serve shenlong:
    root /srv/elstat/shenlong;
    try_files $uri $uri/ index.html;
  }

  location /api {
    # proxy to elstat:
    proxy_pass http://localhost:3333;
  }
}
```
